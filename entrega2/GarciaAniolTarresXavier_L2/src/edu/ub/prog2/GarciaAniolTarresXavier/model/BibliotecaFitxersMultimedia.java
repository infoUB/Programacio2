package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.utils.AplicacioException;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class BibliotecaFitxersMultimedia extends CarpetaFitxers
{
    /**
     * Constructor de la classe BibliotecaFitxersMultimedia
     */
    public BibliotecaFitxersMultimedia()
    {
        super(); //Creearem una instància de CarpetaFitxers
    }
    
    /**
     * Sobreescriptura del mètode addFitxer de CarpetaFitxers
     * Comprova que el fitxer existeixi al disc i no estigui duplicat.
     * No comprova mida màxima
     * @param fitxer Fitxer a afegir
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    @Override
    public void addFitxer(File fitxer) throws AplicacioException
    {
        if (fitxer.exists()) //Si el fitxer existeix al disc
        {
            if(!inFolder(fitxer)) //Si el fitxer no és a biblioteca
            {
                super.carpeta.add(fitxer);
            }
            else
            {
                throw new AplicacioException("El fitxer indicat ja és a la carpeta.");
            }
        }
        else
        {
            throw new AplicacioException("El fitxer indicat no existeix");
        }
    }
        
    /**
     * Metode per comprobar si un fitxer ja es a la biblioteca
     * @param fitxer
     * @return 
     */
    public boolean inFolder(File fitxer)
    {
        Iterator <File> carpetaIterator = carpeta.iterator();
        while (carpetaIterator.hasNext()) //Recorrem tota la carpeta
        {
            File element = carpetaIterator.next();
            if (element.equals(fitxer)) //Si l'actual és igual al paràmetre, ja hi és
            {
                return true;
            }
        }
        return false;
    }
       
    /**
     * Sobreescriptura de toString
     * @return String amb tota la informació dels fitxers que conté l'arrayList, numerats amb un índex.
     */
    @Override
    public String toString()
    {
        String resum = "Biblioteca Fitxers:\n==============\n\n";
        for (int i = 0; i < carpeta.size(); i++)
        {
            resum += "[" + (i+1) + "] " + carpeta.get(i).toString() +"\n";
        }
        return resum;
    }    
}
