package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;

public abstract class FitxerReproduible extends FitxerMultimedia
{
    protected String nom;
    protected String codec;
    protected float durada;
    protected transient Reproductor r;
    /**
     * Constructor de FitxerReproduible
     * @param cami  Path del fitxer
     * @param codec Codecutilitzat pel fitxer
     * @param durada Durada del contingut multimèdia
     * @param r Reproductor
     */
    protected FitxerReproduible(String cami, String codec, float durada, Reproductor r)
    {
        //Cal que demanem el nom? Teòricament l'agafem del path...
        super(cami);
        //this.nom = nom;
        this.codec = codec;
        this.durada = durada;
        this.r = r;
    }
    
    /**
     * Getter del nom
     * @return 
     */
    
    @Override
    public String getNom()
    {
        return this.nom;
    }
    
    /**
     * Getter del codec de l'arxiu multimèdia
     * @return String amb el còdec
     */
    public String getCodec()
    {
        return this.codec;
    }

    /**
     * Getter de la durada de l'arxiu multimèdia
     * @return Float amb la durada
     */
    public float getDurada()
    {
        return this.durada;
    }
    
    /**
     * Sobeescriputra del mètode toString
     * @return String amb la informació comuna dels FitxerReproduible
     */
    @Override
    public String toString()
    {
        return (super.toString() + " Codec: " + this.getCodec() + "\n Durada: " + this.getDurada() + "\n");
    }
    
    protected abstract void reproduir();
    
}
