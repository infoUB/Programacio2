package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;

public class Video extends FitxerReproduible
{
    private int alcada;
    private int amplada;
    private float fps;
    /**
     * Constructor de video
     * @param cami Path del arxiu
     * @param codec Codec de vídeo utilitzat
     * @param durada Durada del vídeo
     * @param alcada Alçada del vídeo (en píxels)
     * @param amplada Amplada del vídeo (en píxels)
     * @param fps Framerate del vídeo en frames per segon (FPS)
     * @param r Reproductor
     */
    public Video(String cami, String codec, float durada, int alcada, int amplada, float fps, Reproductor r)
    {
        super(cami, codec, durada, r);
        this.alcada = alcada;
        this.amplada = amplada;
        this.fps = fps;
    }
    
    /**
     * Geter de la component alçada de la resolució
     * @return Enter amb l'alçada del vídeo en píxels
     */
    public int getAlcada()
    {
        return this.alcada;
    }
    
    /**
     * Geter de la component amplada de la resolució
     * @return Enter amb l'amplada del vídeo en píxels
     */
    public int getAmplada()
    {
        return this.amplada;
    }
    
    /**
     * Geter de la framerate del vídeo
     * @return Float amb els FPS
     */
    public float getFPS()
    {
        return this.fps;
    }
    
    /**
     * Sobreescriptura del mètode toString
     * @return String amb tota la informació de Video
     */
    @Override
    public String toString()
    {
        return (super.toString() + " Resolució: " + this.getAmplada() + "x" + this.getAlcada() + "\n FPS:" + this.getFPS());
    }
    
    /**
     * Sobreescriptura del mètode reproduir de FitxerReproduible
     */
    @Override
    public void reproduir()
    {
        
    }
}
