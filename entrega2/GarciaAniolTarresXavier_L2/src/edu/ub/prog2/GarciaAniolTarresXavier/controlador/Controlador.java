package edu.ub.prog2.GarciaAniolTarresXavier.controlador;
import edu.ub.prog2.GarciaAniolTarresXavier.model.Dades;
import edu.ub.prog2.utils.AplicacioException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public class Controlador 
{
    private Dades dades;
    
    /**
     * Constructor de Controlador
     */
    public Controlador()
    {
        dades = new Dades();
    }
    
    /**
     * Mètode per afegir un vídeo a la biblioteca
     * @param cami Path del arxiu
     * @param descripcio Descripció del vídeo
     * @param codec Codec de vídeo utilitzat
     * @param durada Durada del vídeo
     * @param alcada Alçada del vídeo (en píxels)
     * @param amplada Amplada del vídeo (en píxels)
     * @param fps Framerate del vídeo en frames per segon (FPS)
     * @param r Reproductor
     * @throws AplicacioException 
     */
    public void addVideo(String cami, String descripcio, String codec, float durada, int alcada, int amplada, float fps, Reproductor r ) throws AplicacioException
    {
        try
        {
            dades.addVideo(cami, descripcio, codec, durada, alcada, amplada, fps, r);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    
    /**
     * 
     * @param cami Path de l'àudio
     * @param descripcio Descripció de l'àudio
     * @param fitxerImatge Ftxer de la caràtula de l'àudio
     * @param codec  El còdec de l'àudio
     * @param durada Dudrada de l'àudio
     * @param kbps  Bitrate (qualitat) de l'àudio en kilobits per second (kbps)
     * @param r Reproductor
     * @throws AplicacioException 
     */
    public void addAudio(String cami, String descripcio, File fitxerImatge, String codec, float durada, int kbps, Reproductor r) throws AplicacioException
    {
        try
        {
            dades.addAudio(cami, descripcio, fitxerImatge, codec, durada, kbps, r);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Mètode per eliminar un fitxer donada la seva id
     * @param id Posició que ocupa el fitxer a biblioteca
     * @throws AplicacioException 
     */
    public void removeFitxer(int id) throws AplicacioException
    {
        try
        {
            dades.removeFitxer(id);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Metode que llegeix recupera les dades d'una altra biblioteca.
     * @param path //
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    public void recover(String path) throws AplicacioException
    {
        File file = new File(path);
        try
        {
            dades = Dades.recover(file);
        }
        catch(FileNotFoundException e)
        {
            throw new AplicacioException("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e)
        {
            throw new AplicacioException("IOException: " + e.getMessage());
        }
        catch(AplicacioException e)
        {
            throw new AplicacioException("AplicacioException: " + e.getMessage());
        }
        catch(ClassNotFoundException e)
        {
            throw new AplicacioException("ClassNotFoundException: " + e.getMessage());           
        }
    }
        
    /**
     * Metode que crea un fitxer on guardar les dades actuals. Es crea un fitxer
     * amb extensió .data que despres es podra recuperar.
     * @param fileName nom del fitxer
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    public void save(String fileName) throws AplicacioException
    {
        if (fileName.isEmpty())
        {
            fileName = "MyData";
        }
        File file = new File(fileName + ".dat");
        try
        {
            dades.save(file);
        } 
        catch (AplicacioException e)
        {
            throw new AplicacioException("AplicacioException: " + e.getMessage());
        }
    }
    
    /**
     * Mètode per recopilar tota la informació de cadascun dels fitxers
     * @return List de Strings amb els toString de cada ftxer
     */
    public List<String> mostrarBiblioteca()
    {
        return dades.mostrarBiblioteca();        
    }
    
    /**
     * Sobreescriptura del mètode toString
     * @return String amb tota la informació del controlador (dades)
     */
    @Override
    public String toString()
    {
        return dades.toString();
    }
}
