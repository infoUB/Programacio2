﻿# Pràctica 2!!!

## ToDo:
- [x] Afegir Audio
- [x] Eliminar fitxers
- [x] Atribut de la miniatura del video i audio (crec que es fa amb un atribut)
- [x] Dins del case Mostra demanar que reproduir
- [ ] FitxerReproduible sigui reproduible, juntament amb que Reproductor fasi algo (NO CAL QUE FACI RES!)
- [x] Diria que m'he descuidat comentaris de javadoc

## ToDo Opcional:
- [ ] Funcio per crear menus a gestioAplicacio
- [x] Funcions amb els case de submenus perque no quedi tan plena gestioAplicacio
